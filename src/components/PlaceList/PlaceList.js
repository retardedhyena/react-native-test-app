import React from 'react';
import { StyleSheet, ScrollView, FlatList } from 'react-native';

import ListItem from '../ListItem/ListItem';


const placeList = props => {
    return (
        <FlatList
            data={props.places}
            renderItem={(info) => (
                <ListItem
                    placeName={info.item.name}
                    placeImage={info.item.image}
                    onItemPressed={() => props.onItemSelected(info.item.key)}>
                </ListItem>
            )}
            style={styles.listContainer}>
        </FlatList>
    )
}

const styles = StyleSheet.create({
    listContainer: {
        width: "100%"
    }
})
export default placeList;