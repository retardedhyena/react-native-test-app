import React from 'react';
import { Modal, View, Image, Text, Button, StyleSheet } from 'react-native';

const placeDetail = props => {
    let modalContent = null;

    if (props.selectedPlace) {
        modalContent = (
            <View>
                <Image
                    source={props.selectedPlace.image}
                    style={styles.placeImage} />
                <Text
                    style={styles.placeName}
                >{props.selectedPlace.name}</Text>
            </View>
        )
    }

    return (
        <Modal
            onRequestClose={props.onModalClosed}
            visible={props.selectedPlace !== null}
            animationType="slide">
            <View style={styles.modalContainer}>
                {modalContent}
                <View>
                    <Button title="delete" color="red"
                        onPress={props.onItemDeleted} />
                    <Button title="close"
                        onPress={props.onModalClosed} />
                </View>
            </View>
        </Modal>
    )
};

const styles = StyleSheet.create({
    modalContainer: {
        margin: 20,
    },
    placeImage: {
        height: 200,
        width: "100%"
    },
    placeName: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 30
    }
})

export default placeDetail;